# sis-footer

A footer view component for Table and Charts.

## setup

- install: `npm i sis-footer` (with the nexus configuration)
- live: `./node_modules/web-component-env/wce start`

## Usage

To know how to use the component, have a look to `app.js`.

Why?
- because `app.js` is used to show component features and should cover basic and complex needs
- because `app.js` will be updated if the code is updated

## API

To know the API see `PropTypes`.

Why?
- because `PropTypes` will be updated since it's part of the code
- because `PropTypes` are always formatted in the same way
- because `PropTypes` also increase maintainability
