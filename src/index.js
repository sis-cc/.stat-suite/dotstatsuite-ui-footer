import meta from '../package.json';
console.info(`${meta.name}@${meta.version}`);

import React from 'react';
import PropTypes from 'prop-types';
import { get, isNumber, isString } from 'lodash';
import { Classes, Intent, Position, Popover, PopoverInteractionKind } from '@blueprintjs/core';
import '@blueprintjs/core/dist/blueprint.css';
import {
  CopyrightIcon,
  CopyrightTooltipContent,
  StyledFooter,
  Logo,
  Legend,
  Meta,
  PopoverStyle,
  Resource,
  Source,
  Typography,
  spacing,
  SrOnly
} from './glamorous-components';

const Footer = ({ fonts, legend, logo, owner, source, terms, width }) => {
  let legendComponent = null;
  if (legend) legendComponent = <Legend width={width} >{legend}</Legend>;


  const sourceComponent = (source) => {
    if (!source || !source.link)
      return null;
    const { label, link } = source;
    return (
      <Source fonts={get(fonts, 'source', {})} href={link} target="_blank" width={width}>{label ? label : link}</Source>
    );
  }
  const link = (terms && terms.link) 
    ? <a tabIndex={-1} href={terms.link} target="_blank">
        {terms.label ? terms.label : terms.link}
      </a>
    : null

  const copyrightComponent = (owner, terms, link) => {
    if (!owner && (!terms || !terms.link))
      return null;
    const content =
      <CopyrightTooltipContent tabIndex={-1} fonts={get(fonts, 'copyright', {})}>
        <Typography tabIndex={-1} className={spacing}>{owner}</Typography>
        {link}
      </CopyrightTooltipContent>;
    return (
      <Popover
        content={content}
        popoverClassName={`${PopoverStyle} ${Classes.MINIMAL}`}
        hoverCloseDelay={50}
        hoverOpenDelay={0}
        interactionKind={PopoverInteractionKind.HOVER}
        openOnTargetFocus={true}
        position={Position.TOP}
        isModal={false}
        autoFocus={false}
        enforceFocus={false}
        tetherOptions={{
          constraints: [
            { to: 'window', pin: true },
          ]
        }}
      >
        <CopyrightIcon fonts={get(fonts, 'copyright', {})} width={width}>©</CopyrightIcon>
      </Popover>
    )
  }

  let logoComponent = null;
  if (logo) {
    if (isString(logo)) logoComponent = <Logo footerWidth={width}><img src={logo} /></Logo>;
    else logoComponent = <Logo footerWidth={width}>{logo}</Logo>;
  }

  return (
    <StyledFooter width={width}>
      {legendComponent}
      <Meta>
        {copyrightComponent(owner, terms, link)}
        <SrOnly tabIndex={0}>{owner}</SrOnly>
        <SrOnly tabIndex={0}>{link}</SrOnly>
        <Resource>
          {sourceComponent(source)}
          {logoComponent}
        </Resource>
      </Meta>
    </StyledFooter>
  );
}

Footer.propTypes = {
  fonts: PropTypes.object,
  legend: PropTypes.element,
  logo: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
  ]),
  owner: PropTypes.string,
  source: PropTypes.shape({
    label: PropTypes.string,
    link: PropTypes.string
  }),
  terms: PropTypes.shape({
    label: PropTypes.string,
    link: PropTypes.string
  }),
  width: PropTypes.number
};

export default Footer;
