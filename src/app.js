import React, { useState } from 'react';
import { render } from 'react-dom';
import { Helmet } from 'react-helmet';
import Footer from '.';
import './assets/app.less';
import logo from './assets/logo.png';

const Legend = () => <div className="legend">legend</div>;
const link = 'http://www.google.com';
const source = { label: 'source label', link };
const sourceWithoutLabel = { link };
const longSource = { label: 'a label for source link can also be long', link };
const logoAsString = 'https://upload.wikimedia.org/wikipedia/commons/3/30/Googlelogo.png';
const logoAsElementFromString = <img src={logoAsString} />;
const logoAsElementFromImage = <img src={logo} />;
const LongLegend = () => <div className="legend">a very very very long legend. Like very long .... Who knew it could be so long ?</div>; 
const owner = "©OECD";
const longOwner = "©OECD verylong,long,long"
const terms = { label: 'Terms & Conditions', link: 'https://www.oecd.org/termsandconditions/' };
const termsWithoutLabel = { link: 'https://www.oecd.org/termsandconditions/' }
const longTerms = { label: 'long long long long Terms & Conditions', link: 'https://www.oecd.org/termsandconditions/' };

const App = () => {
  const [isRtl, setRtl] = useState(false);

  return (
    <div className='app'>
      <Helmet htmlAttributes={{ dir: isRtl ? 'rtl' : 'ltr' }} />
      <button onClick={() => setRtl(!isRtl)}>{isRtl ? 'left to right' : 'right to left'}</button>
      <h1 className='title' > Background color of the footer set as gray for this demo</h1>
      <div className='usecase'>
        <h2>no props</h2><Footer />
      </div>
      <div className='usecase'>
        <h2>only legend</h2><Footer legend={<Legend />} />
      </div>
      <div className='usecase'>
        <h2>only logo (string)</h2><Footer logo={logoAsString} />
      </div>
      <div className='usecase'>
        <h2>only logo (element from string)</h2><Footer logo={logoAsElementFromString} />
      </div>
      <div className='usecase'>
        <h2>only logo (element from image)</h2><Footer logo={logoAsElementFromImage} />
      </div>
      <div className='usecase'>
        <h2>only source</h2><Footer source={source} />
      </div>
      <div className='usecase'>
        <h2>only source (no label)</h2><Footer source={sourceWithoutLabel} />
      </div>
      <div className='usecase'>
        <h2>only owner</h2><Footer owner={owner} />
      </div>
      <div className='usecase'>
        <h2>only terms</h2><Footer terms={terms} />
      </div>
      <div className='usecase'>
        <h2>only terms (no label)</h2><Footer terms={termsWithoutLabel} />
      </div>
      <div className='usecase'>
        <h2>legend and source</h2><Footer legend={<Legend />} source={source} />
      </div>
      <div className='usecase'>
        <h2>legend and logo</h2><Footer legend={<Legend />} logo={logoAsElementFromImage} />
      </div>
      <div className='usecase'>
        <h2>source and logo</h2><Footer source={source} logo={logoAsElementFromImage} />
      </div>
      <div className='usecase'>
        <h2>legend, logo and source</h2><Footer legend={<Legend />} source={source} logo={logoAsElementFromImage} />
      </div>
      <div className='usecase'>
        <h2>long legend, logo and source</h2>
        <Footer legend={<LongLegend />} logo={logoAsElementFromImage} source={source} />
      </div>
      <div className='usecase'>
        <h2>long legend, long source and logo</h2>
        <Footer legend={<LongLegend />} logo={logoAsElementFromImage} source={longSource} />
      </div>
      <div className='usecase'>
        <h2>legend, logo terms, owner and source</h2>
        <Footer legend={<Legend />} logo={logoAsElementFromImage} source={source} owner={owner} terms={terms} />
      </div>
      <div className='usecase'>
        <h2>All long</h2>
        <Footer legend={<LongLegend />} logo={logoAsElementFromImage} source={longSource} owner={longOwner} terms={longTerms} />
      </div>
    </div>
  )
};

render(<App />, document.getElementById('root'));