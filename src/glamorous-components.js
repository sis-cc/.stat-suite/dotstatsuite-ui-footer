import glamorous from 'glamorous';
import { css } from 'glamor';
import { isNumber } from 'lodash';

export const Legend = glamorous.div(
  {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start'
  },
  ({ width }) => {
    if (!isNumber(width)) {
      return ({});
    }
    else if (width < 370) {
      return ({ display: 'none' });
    }
  }
);

export const Logo = glamorous.div(
  {
    marginBottom: 5,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5,
    width: 100,
    '& img': {
      width: '100%',
      height: '100%'
    }
  },
  ({ footerWidth }) => {
    if (!isNumber(footerWidth))
      return ({});
    else if (footerWidth < 120) {
      return ({
        width: 0,
        marginBottom: 0,
        marginLeft: 0,
        marginRight: 0,
        marginTop: 0,
      });
    }
    else if (footerWidth <= 270) {
      return ({
        width: 60,
        marginBottom: 1,
        marginLeft: 6,
        marginRight: 6,
        marginTop: 1,
      });
    }
    else if (footerWidth <= 370) {
      return ({
        width: 75,
        marginBottom: 2,
        marginLeft: 7,
        marginRight: 7,
        marginTop: 2,
      });
    }
    else if (footerWidth <= 760) {
      return ({ width: 100 });
    }
    else if (footerWidth <= 855) {
      return ({ width: 125 });
    }
    else {
      return ({ width: 150 });
    }
  }
);

export const Source = glamorous.a(
  {
    color: '#7A7A7A', textAlign: 'right',
    fontFamily: 'bernino-sans-narrow-regular',
    fontSize: 16,
    textDecoration: 'none',
    '&:hover': {
      color: '#0297C9'
    }
  },
  ({ fonts = {}, width }) => {
    if (!isNumber(width))
      return ({ ...fonts });
    else if (width < 370) {
      return ({ ...fonts, fontSize: 0 });
    }
    else if (width <= 420) {
      return ({ ...fonts, fontSize: 12 });
    }
    else if (width <= 855) {
      return ({ ...fonts, fontSize: 14 });
    }
    else {
      return ({ ...fonts, fontSize: 16 });
    }
  }
);

export const Resource = glamorous.div(
  {
    alignItems: 'center',
    display: 'flex',
    flexGrow: 1,
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'flex-end'
  }
);

export const SrOnly = glamorous.p(
  {
    position: 'absolute',
    width: 1,
    height: 1,
    padding: 0,
    margin: 1,
    overflow: 'hidden',
    clip: 'rect(0, 0, 0, 0)',
    whiteSpace: 'nowrap',
    border: 0,
  }
);

export const CopyrightTooltipContent = glamorous.div(
  {
    display: 'flex',
    backgroundColor: '#30404D',
    borderRadius: 3,
    boxSizing: 'border-box',
    fontFamily: 'bernino-sans-narrow-regular',
    fontSize: 16,
    padding: 5,
    textAlign: 'left',
    '& span': {
      color: 'white',
      marginRight: 5
    },
    '& a': {
      color: '#0297C9',
      textDecoration: 'none',
      '&:hover': {
        color: '#7A7A7A'
      }
    }
  },
  ({ fonts = {} }) => ({
    ...fonts
  })
);

export const CopyrightIcon = glamorous.span(
  {
    color: '#7A7A7A',
    fontFamily: 'bernino-sans-narrow-regular',
    fontSize: 16,
    justifyContent: 'flex-start',
    marginLeft: 10,
    textAlign: 'left'
  },
  ({ fonts = {}, width }) => {
    if (!isNumber(width))
      return ({ ...fonts });
    else if (width < 120) {
      return ({ ...fonts, display: 'none' });
    }
    else if (width <= 560) {
      return ({ ...fonts, fontSize: 12 });
    }
    else if (width <= 855) {
      return ({ ...fonts, fontSize: 14 });
    }
    else {
      return ({ ...fonts, fontSize: 16 });
    }
  }
);

export const Meta = glamorous.div(
  {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'space-between'
  }
);

export const StyledFooter = glamorous.div(
  {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    paddingBottom: 5,
    paddingTop: 5
  },
  ({ width }) => {
    if (!isNumber(width))
      return ({});
    if (width < 120)
      return ({ display: 'none' });
  }
);

export const PopoverStyle = css(
  {
    boxShadow: '0px 0px white'
  }
);

export const Typography = glamorous.p({
  color: 'white',
})

export const spacing = css({
  marginRight: 5,
  marginLeft: 5
});